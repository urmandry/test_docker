FROM ruby:3.2.2

# Working directory
WORKDIR /app

# Files Gemfile and Gemfile.lock
COPY Gemfile Gemfile.lock ./

# Bundler
RUN gem install bundler
RUN bundle install

#Copy
COPY . .

#The start command for app
CMD ["rails", "server", "-b", "0.0.0.0"]
# Description

This is a Ruby on Rails application that was Dockerized

# Steps that I followed

1. Install Docker and Docker-Compose (I used the terminal commands from oficial documentation of this applications).

2. Cloned the repo with the existent Ruby on Rails project the command: git clone https://github.com/alexeymezenin/ruby-on-rails-realworld-example-app

3. Enitialized the Dockerfile (specifying: ruby version, working directory, copying Gemfile and Gemfile.lock, mentioned installing bundler and the start command for app)

4. Made the docker image, I used in the terminal command: docker build -t image-name:tag . For my project was: docker build -t test-ab-rubyonrailsrealworldexampleapp:isv3-48-test-ab .

5. I instaled an configured the database, PostgreSQL.

6. After this steps I created the docker-compose.yml file and configured it adding information like: docker-image, port and setting up the environment for database.

7. I runned the containers using command: sudo docker-compose up (Here I have some problems with the 'secret_key_base' for 'production' environment and I didn't manage how to fix that problem)

8. For migrate the schemas I used command: sudo docker-compose run app rails db:migrate

9. For adding Seed data the command: docker-compose run app rails db:seed